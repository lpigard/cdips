module purge
module load Intel
module load IntelMPI
module load HDF5
#module load h5py/2.8.0-Python-3.6.6-serial
module load CMake

### for python scripts
#module load SciPy-Stack/2019a-Python-3.6.8
#module load h5py
###

export CC=icc
