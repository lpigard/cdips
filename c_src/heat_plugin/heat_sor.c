/* Copyright (C) 2017-2018 Ludwig Schneider
Copyright (C) 2017 De-Wen Sun

This file is part of SOMA.

SOMA is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SOMA is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with SOMA.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "heat_io.h"
#include "heat_sor.h"

uint64_t successive_over_relaxation(Heat * const h, Phase * const p)
{
    printf("INFO: Iterate with successive over-relaxation and constant temperature.\n");
    printf("INFO: Cold region: x, y, z, radius = %d, %d, %d, %d\n", h->cold_area.x[0], h->cold_area.x[1], h->cold_area.x[2], h->cold_area.radius);
    printf("INFO: Hot region: x, y, z, radius = %d, %d, %d, %d\n", h->hot_area.x[0], h->hot_area.x[1], h->hot_area.x[2], h->hot_area.radius);

    // first loop without recalculating the fields from bead data; it is faster but precision is lowered for each step by order of machine precision 
    for(p->time = 0, p->start_time = 0; continue_loop(h, p) == 1; p->time++)
    {
        create_checkpoint(h, p);
        update_beads_T(h, p);
        //calc_fields_T(h, p);    // could in principle
        //calc_theta(h, p);       // be skipped skipped
        screen_output(p, p->args.timesteps_arg == 0 ? 2 * p->time : p->args.timesteps_arg);
    }

    // second loop with exact recalculation of fields from bead data
    for(int t = 0; continue_loop(h, p) == 1 || t < 3; t++, p->time++)
    {
        create_checkpoint(h, p);
        update_beads_T(h, p);
        calc_fields_T(h, p); 
        calc_theta(h, p);      
        screen_output(p, p->args.timesteps_arg == 0 ? 2 * p->time : p->args.timesteps_arg);
    }

    printf("INFO: I ran for %u steps\n", p->time);

    return p->time;
}

int continue_loop(Heat * const h, const Phase * const p)
{
    if((h->deviation > h->epsilon || p->time < 3) && (p->args.timesteps_arg == 0 || p->time < p->args.timesteps_arg))
    {
        return 1;
    }
    else
    {
        return 0;
    }
}

void update_beads_T(Heat * const h, const Phase * const p)
{
    soma_scalar_t deviation = 0.;

    for (uint64_t i = 0; i < p->n_polymers; i++)
    {
        const unsigned int N = p->poly_arch[ p->poly_type_offset[p->polymers[i].type]];
        for (unsigned int j = 0; j < N; j++)
        {
            const unsigned int monotype = get_particle_type(p->poly_arch[ p->poly_type_offset[p->polymers[i].type] + 1 + j]);
            int x, y, z;
            coord_to_cell_coordinate(p, p->polymers[i].beads[j].x, p->polymers[i].beads[j].y, p->polymers[i].beads[j].z, &x, &y, &z);
            const uint64_t cell = cell_coordinate_to_index(p, x, y, z);
            uint64_t index = cell_to_index_unified(p, cell, monotype);

            const unsigned int ij = get_2d_index(i, j, h->max_n_beads);

            soma_scalar_t sum_kappa_theta = 0.;
            soma_scalar_t sum_kappa_nu = 0.;

            // non bonded conduction
            for(unsigned int type = 0; type < p->n_types; type++)
            {
                const unsigned int index_neighbor = cell_to_index_unified(p, cell, type);
                sum_kappa_theta += h->kappa_non_bonded[monotype * p->n_types + type] * h->theta[index_neighbor];
                sum_kappa_nu += h->kappa_non_bonded[monotype * p->n_types + type] * h->nu[index_neighbor];
            }

            soma_scalar_t sum_kappa = 0.;
            soma_scalar_t sum_kappa_T = 0.;

            // bonded conduction
            const int start = get_bondlist_offset(p->poly_arch[p->poly_type_offset[p->polymers[i].type] + j + 1]);
            if(start > 0)
            {
                int i0 = start;
                unsigned int end;
                do
                {
                    const uint32_t info = p->poly_arch[i0++];
                    end = get_end( info );
                    const int offset = get_offset( info);
                    const unsigned int neighbour_id = j + offset;
                    const unsigned int neighbour_type = get_particle_type(p->poly_arch[p->poly_type_offset[p->polymers[i].type] + neighbour_id + 1]);
                    const const unsigned int ij_neighbor = get_2d_index(i, neighbour_id, h->max_n_beads);

                    sum_kappa += h->kappa_bonded[monotype * p->n_types + neighbour_type];
                    sum_kappa_T += h->kappa_bonded[monotype * p->n_types + neighbour_type] * h->beads_T[ij_neighbor];
                }while(end==0);
            }

            const soma_scalar_t dT = h->omega * ((sum_kappa_theta + sum_kappa_T) / (sum_kappa_nu + sum_kappa) - h->beads_T[ij]);

            int on_fixed_area = 0;
            if(is_on_fixed_area(h, p, x, y, z, &h->hot_area) == 1 || is_on_fixed_area(h, p, x, y, z, &h->cold_area) == 1)
            {
                on_fixed_area = 1;
            }

            if(on_fixed_area == 0)
            {
                const soma_scalar_t d = fabs(sum_kappa_theta + sum_kappa_T - h->beads_T[ij] * (sum_kappa_nu + sum_kappa));
                if(d > deviation)
                {
                    deviation = d;
                }
                h->beads_T[ij] += dT;

                // update temperature field needed for subsequent successive-over relaxation  step
                h->theta[calc_field_index(p, x,     y,     z,     monotype)] += dT;
                h->theta[calc_field_index(p, x + 1, y,     z,     monotype)] += dT;
                h->theta[calc_field_index(p, x - 1, y,     z,     monotype)] += dT;
                h->theta[calc_field_index(p, x,     y + 1, z,     monotype)] += dT;
                h->theta[calc_field_index(p, x,     y - 1, z,     monotype)] += dT;
                h->theta[calc_field_index(p, x,     y,     z + 1, monotype)] += dT;
                h->theta[calc_field_index(p, x,     y,     z - 1, monotype)] += dT;
            }
        }
    }
    h->deviation = deviation;

}

void calc_fields_T(Heat * const h, const Phase * const p)
{
    for (uint64_t index = 0; index < p->n_types*p->n_cells; index++)
    {
        h->fields_T[index] = 0.;
    }

    for (uint64_t i = 0; i < p->n_polymers; i++)
    {
        const unsigned int N = p->poly_arch[ p->poly_type_offset[p->polymers[i].type]];
        for (unsigned int j = 0; j < N; j++)
        {
            const unsigned int monotype = get_particle_type(p->poly_arch[ p->poly_type_offset[p->polymers[i].type]+1+j]);
            const unsigned int index = coord_to_index_unified(p, p->polymers[i].beads[j].x, p->polymers[i].beads[j].y, p->polymers[i].beads[j].z, monotype);
            const unsigned int ij = get_2d_index(i, j, h->max_n_beads);
            h->fields_T[index] += h->beads_T[ij];
        }
    }
}

void calc_theta(Heat * const h, const Phase * const p)
{
    for(int type = 0; type < p->n_types; type++)
    {
        for(int x = 0; x < p->nx; x++)
        {
            for(int y = 0; y < p->ny; y++)
            {
                for(int z = 0; z < p->nz; z++)
                {
                    const int i0 = calc_field_index(p, x, y, z, type);
                    h->theta[i0] = h->fields_T[i0];
                    h->theta[i0] += h->fields_T[calc_field_index(p, x + 1, y,     z,     type)];
                    h->theta[i0] += h->fields_T[calc_field_index(p, x - 1, y,     z,     type)];
                    h->theta[i0] += h->fields_T[calc_field_index(p, x,     y + 1, z,     type)];
                    h->theta[i0] += h->fields_T[calc_field_index(p, x,     y - 1, z,     type)];
                    h->theta[i0] += h->fields_T[calc_field_index(p, x,     y,     z + 1, type)];
                    h->theta[i0] += h->fields_T[calc_field_index(p, x,     y,     z - 1, type)];
                }
            }
        }
    }
}

void calc_nu(Heat * const h, const Phase * const p)
{
    for(int type = 0; type < p->n_types; type++)
    {
        for(int x = 0; x < p->nx; x++)
        {
            for(int y = 0; y < p->ny; y++)
            {
                for(int z = 0; z < p->nz; z++)
                {
                    const int i0 = calc_field_index(p, x, y, z, type);
                    h->nu[i0] = p->fields_32[i0];
                    h->nu[i0] += p->fields_32[calc_field_index(p, x + 1, y,     z,     type)];
                    h->nu[i0] += p->fields_32[calc_field_index(p, x - 1, y,     z,     type)];
                    h->nu[i0] += p->fields_32[calc_field_index(p, x,     y + 1, z,     type)];
                    h->nu[i0] += p->fields_32[calc_field_index(p, x,     y - 1, z,     type)];
                    h->nu[i0] += p->fields_32[calc_field_index(p, x,     y,     z + 1, type)];
                    h->nu[i0] += p->fields_32[calc_field_index(p, x,     y,     z - 1, type)];
                }
            }
        }
    }
}

