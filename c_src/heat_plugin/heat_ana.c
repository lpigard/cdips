/* Copyright (C) 2017-2018 Ludwig Schneider
Copyright (C) 2017 De-Wen Sun

This file is part of SOMA.

SOMA is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SOMA is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with SOMA.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "heat_io.h"
#include "heat_ana.h"

void do_ana(Heat * const h, const Phase * const p)
{
    printf("Do analysis of stationary temperature field.\n");
    calc_T(h ,p);
    calc_q1_and_q2(h, p);
    calc_qhot_and_qcold(h, p);
    calc_j1_and_j2(h, p);

    if(fabs(fabs(h->qhot) - fabs(h->q1) - fabs(h->q2)) > h->epsilon_ana)
    {
        printf("ERROR: Energy not conserved (%lg != %lg + %lg)!\n", fabs(h->qhot), fabs(h->q1), fabs(h->q2));
    }
}

void calc_q1_and_q2(Heat * const h, const Phase * const p)
{
    const int nx[3] = {p->nx, p->ny, p->nz};
    const int n = nx[h->normal_direction];
    soma_scalar_t * q = (soma_scalar_t *) malloc(n * sizeof(soma_scalar_t));

    for(int c = 0; c < n; c++)
    {
        q[c] = 0.;
    }

    for (uint64_t i = 0; i < p->n_polymers; i++)
    {
        const unsigned int N = p->poly_arch[ p->poly_type_offset[p->polymers[i].type]];
        for (unsigned int j = 0; j < N; j++)
        {
            const unsigned int monotype = get_particle_type(p->poly_arch[ p->poly_type_offset[p->polymers[i].type] + 1 + j]);

            int x[3];
            coord_to_cell_coordinate(p, p->polymers[i].beads[j].x, p->polymers[i].beads[j].y, p->polymers[i].beads[j].z, &x[0], &x[1], &x[2]);
            const uint64_t cell = cell_coordinate_to_index(p, x[0], x[1], x[2]);
            uint64_t index = cell_to_index_unified(p, cell, monotype);

            const unsigned int ij = get_2d_index(i, j, h->max_n_beads);

            soma_scalar_t q_nb = 0.;

            // non bonded conduction
            for(unsigned int type = 0; type < p->n_types; type++)
            {
                const soma_scalar_t kappa = h->kappa_non_bonded[monotype * p->n_types + type];
                unsigned int index_neighbor;

                switch(h->normal_direction)
                {
                    case 0: index_neighbor = calc_field_index(p, x[0] + 1, x[1],     x[2],     type); break;
                    case 1: index_neighbor = calc_field_index(p, x[0],     x[1] + 1, x[2],     type); break;
                    case 2: index_neighbor = calc_field_index(p, x[0],     x[1],     x[2] + 1, type); break;
                    default: break;
                }

                q_nb += kappa * (h->fields_T[index_neighbor] - p->fields_32[index_neighbor] * h->beads_T[ij]);
            }

            const int c0 = x[h->normal_direction];
            q[c0] += q_nb;

            // bonded conduction
            const int start = get_bondlist_offset(p->poly_arch[p->poly_type_offset[p->polymers[i].type] + j + 1]);
            if(start > 0)
            {
                int i0 = start;
                unsigned int end;
                do
                {
                    const uint32_t info = p->poly_arch[i0++];
                    end = get_end( info );
                    const int offset = get_offset( info);
                    const unsigned int neighbour_id = j + offset;
                    const unsigned int neighbour_type = get_particle_type(p->poly_arch[p->poly_type_offset[p->polymers[i].type] + neighbour_id + 1]);
                    const const unsigned int ij_neighbor = get_2d_index(i, neighbour_id, h->max_n_beads);
                    int x_neighbor[3];
                    coord_to_cell_coordinate(p, p->polymers[i].beads[neighbour_id].x, p->polymers[i].beads[neighbour_id].y, p->polymers[i].beads[neighbour_id].z, &x_neighbor[0], &x_neighbor[1], &x_neighbor[2]);

                    const soma_scalar_t q_b = h->kappa_bonded[monotype * p->n_types + neighbour_type] * (h->beads_T[ij_neighbor] - h->beads_T[ij]);

                    const int cn = x_neighbor[h->normal_direction];

                    int dc = cn - c0;
                    
                    // apply minimum image convention
                    if(dc > n / 2)
                    {
                        dc -= n;
                    }
                    if(dc <= -n / 2)
                    {
                        dc += n; 
                    }

                    soma_scalar_t x0, xn;

                    switch(h->normal_direction)
                    {
                        case 0:
                        x0 = p->polymers[i].beads[j].x;
                        xn = p->polymers[i].beads[neighbour_id].x;
                        break;

                        case 1:
                        x0 = p->polymers[i].beads[j].y;
                        xn = p->polymers[i].beads[neighbour_id].y;
                        break;

                        case 2:
                        x0 = p->polymers[i].beads[j].z;
                        xn = p->polymers[i].beads[neighbour_id].z;
                        break;
                    }

                    if(xn > x0)
                    //if(dc > 0)
                    {
                        for(int c = c0; c != cn; c = (c + 1) % n)
                        {
                            q[c] += q_b;
                        }
                    }   
                }while(end==0);
            }
        }
    }

    const int c1 = h->hot_area.x[h->normal_direction];
    const int c2 = h->cold_area.x[h->normal_direction];

    int low = c1;
    int high = c2;

    if(low > high)
    {
        low = c2;
        high = c1;
    }

    // check if flux is constant on both paths
    h->q1 = q[low];
    h->q2 = q[high];
    soma_scalar_t max_error = 0.;

    for(int c = 0; c < n; c++)
    {
        soma_scalar_t error = 0.;

        if(c >= low && c < high)
        {
            error = fabs(q[c] - h->q1);
        }
        else
        {
            error = fabs(q[c] - h->q2);
        }

        if(error > max_error)
        {
            max_error = error;
        }
    }

    if(max_error > h->epsilon_ana)
    {
        printf("ERROR: Energy not conserved for q1 and q2 (%lg)! q along normal direction:\n", max_error);
        for(int c = 0; c < n; c++)
        {
            printf("%lg\n", q[c]);
        }
    }

    free(q);
}

void calc_qhot_and_qcold(Heat * const h, const Phase * const p)
{
    h->qhot = 0.;
    h->qcold = 0.;

    for (uint64_t i = 0; i < p->n_polymers; i++)
    {
        const unsigned int N = p->poly_arch[ p->poly_type_offset[p->polymers[i].type]];
        for (unsigned int j = 0; j < N; j++)
        {
            const unsigned int monotype = get_particle_type(p->poly_arch[ p->poly_type_offset[p->polymers[i].type] + 1 + j]);
            int x, y, z;
            coord_to_cell_coordinate(p, p->polymers[i].beads[j].x, p->polymers[i].beads[j].y, p->polymers[i].beads[j].z, &x, &y, &z);
            const uint64_t cell = cell_coordinate_to_index(p, x, y, z);
            uint64_t index = cell_to_index_unified(p, cell, monotype);

            const unsigned int ij = get_2d_index(i, j, h->max_n_beads);

            soma_scalar_t sum_kappa_theta = 0.;
            soma_scalar_t sum_kappa_nu = 0.;

            // non bonded conduction
            for(unsigned int type = 0; type < p->n_types; type++)
            {
                const unsigned int index_neighbor = cell_to_index_unified(p, cell, type);
                sum_kappa_theta += h->kappa_non_bonded[monotype * p->n_types + type] * h->theta[index_neighbor];
                sum_kappa_nu += h->kappa_non_bonded[monotype * p->n_types + type] * h->nu[index_neighbor];
            }

            soma_scalar_t sum_kappa = 0.;
            soma_scalar_t sum_kappa_T = 0.;

            // bonded conduction
            const int start = get_bondlist_offset(p->poly_arch[p->poly_type_offset[p->polymers[i].type] + j + 1]);
            if(start > 0)
            {
                int i0 = start;
                unsigned int end;
                do
                {
                    const uint32_t info = p->poly_arch[i0++];
                    end = get_end( info );
                    const int offset = get_offset( info);
                    const unsigned int neighbour_id = j + offset;
                    const unsigned int neighbour_type = get_particle_type(p->poly_arch[p->poly_type_offset[p->polymers[i].type] + neighbour_id + 1]);
                    const const unsigned int ij_neighbor = get_2d_index(i, neighbour_id, h->max_n_beads);

                    sum_kappa += h->kappa_bonded[monotype * p->n_types + neighbour_type];
                    sum_kappa_T += h->kappa_bonded[monotype * p->n_types + neighbour_type] * h->beads_T[ij_neighbor];
                }while(end==0);
            }

            const soma_scalar_t q = sum_kappa_theta + sum_kappa_T - h->beads_T[ij] * (sum_kappa_nu + sum_kappa);

            if(is_on_fixed_area(h, p, x, y, z, &h->hot_area))
            {
                h->qhot += q;
            }

            if(is_on_fixed_area(h, p, x, y, z, &h->cold_area))
            {
                h->qcold += q;
            }
        }
    }

    if(fabs(h->qhot + h->qcold) > h->epsilon_ana)
    {
        printf("ERROR: Energy not conserved for qhot (%lg) and qcold (%lg).\n", h->qhot, h->qcold);
    }
}

void calc_j1_and_j2(Heat * const h, const Phase * const p)
{
    soma_scalar_t a = 1.;
    const int nx[3] = {p->nx, p->ny, p->nz};
    for(int d = 0; d < 3; d++)
    {
        if(d != h->normal_direction)
        {
            a *= nx[d];
        }
    }

    h->j1 = h->q1 / a;
    h->j2 = h->q2 / a;

}

void calc_T(Heat * const h, const Phase * const p)
{
    for(uint64_t cell = 0; cell < p->n_cells; cell++)
    {
        h->T[cell] = 0.;
        uint64_t n = 0;

        for(unsigned int type = 0; type < p->n_types; type++)
        {
            const uint64_t index = cell_to_index_unified(p, cell, type);
            n += p->fields_32[index];
            h->T[cell] += h->fields_T[index];
        }
        h->T[cell] /= n;
    }
}
