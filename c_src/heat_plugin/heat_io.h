/* Copyright (C) 2017-2018 Ludwig Schneider
Copyright (C) 2017 De-Wen Sun

This file is part of SOMA.

SOMA is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SOMA is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with SOMA.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef HEAT_IO_H
#define HEAT_IO_H

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "io.h"
#include "mc.h"
#include "mesh.h"
#include "mpiroutines.h"
#include "init.h"
#include "test.h"
#include "ana.h"
#include "rng.h"
#include "walltime.h"
#include "generate_positions.h"
#include "polytype_conversion.h"

/*! file heat_io.h
\brief File declaring functions to handle all basic data structures, io and other stuff.
*/

//! Struct area for which the temperature is held constant
typedef struct
{
    //! position vector of center of mass of area
    int x[3];
    //! radius of region (circle); if -1 then infinitely extended
    int radius;
} Area;

//! Struct combining all data.
typedef struct
{
    //! iteration steps
    int time;
    //! maximum number of beads in any polymer
    unsigned int max_n_beads;
    //! relaxation parameter for SOR
    soma_scalar_t omega;
    //! number of time steps between checkpoints
    unsigned int dt_check;
    //! deviation from stationary temperature field
    soma_scalar_t deviation;
    //! error margin for stationary temperature field
    soma_scalar_t epsilon;
    //! error margin for error checking in analysis
    soma_scalar_t epsilon_ana;
    //! time step at which the next check point happens.
    uint64_t t_check;
    //! Array which holds the temperature of each bead
    soma_scalar_t *beads_T;
    //! Array which holds the sum of temperatures of all beads for a given cell and type
    soma_scalar_t *fields_T;
    //! Array which holds the sum of temperatures of all beads for a given cell and type and its neighboring cells
    soma_scalar_t *theta;
    //! Array which holds the number of particles of each cell and type
    unsigned int *nu;
    //! Array which holds hopping rates kappa_bonded (n_types * n_types)
    soma_scalar_t *kappa_bonded;
    //! Array which holds hopping rates kappa_non_bonded (n_types * n_types)
    soma_scalar_t *kappa_non_bonded;
    //! Array which holds the normalized temperature field
    soma_scalar_t *T;
    //! first heat flux along normal_direction
    soma_scalar_t j1;
    //! second heat flux along normal_direction
    soma_scalar_t j2;
    //! first heat flux along normal_direction
    soma_scalar_t q1;
    //! second heat flux along normal_direction
    soma_scalar_t q2;
    //! energy rate needed to retain hot area
    soma_scalar_t qhot;
    //! energy rate needed to retain cold area; it should hold |q_hot| = |q_cold| = |q1| + |q2| because of energy conservation
    soma_scalar_t qcold;
    //! direction of normal of hot and cold areas; 0, 1, 2 = x , y, z
    int normal_direction;
    //! struct that contains information on hot area
    Area hot_area;
    //! struct that contains information on cold area
    Area cold_area;
}Heat;

void create_checkpoint(Heat * const h, const Phase * const p);

int init_heat(Heat * const heat, const Phase * const p);

int free_heat(Heat * const h, const Phase * const p);

int write_temperature_data(const char*const filename, const Phase *const p, Heat *const h);

void write_dataset(hid_t file_id, const void *data, const char *name, int rank, const hsize_t * current_dims, hid_t dtype_id);

void write_attribute(hid_t file_id, const char *dset_name, const void *data, const char *name, int rank, const hsize_t * current_dims, hid_t dtype_id);

unsigned int get_2d_index(const unsigned int poly,const unsigned int mono, const unsigned int max_n_beads);

uint64_t calc_field_index(const Phase * const p, int x, int y, int z, const int type);

int read_heat_parameters(const char * const filename, const Phase *const p, Heat *const h);

void read_dataset(hid_t file_id, void *data, const char *name, hid_t dtype_id);

void read_attribute(hid_t file_id, void *data, const char *name, hid_t dtype_id);

int is_on_fixed_area(Heat * const h, const Phase * const p, const int cx, const int cy, const int cz, const Area * const a);

#endif//HEAT_IO_H
