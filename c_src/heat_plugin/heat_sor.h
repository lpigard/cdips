/* Copyright (C) 2017-2018 Ludwig Schneider
Copyright (C) 2017 De-Wen Sun

This file is part of SOMA.

SOMA is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SOMA is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with SOMA.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef HEAT_SOR_H
#define HEAT_SOR_H

uint64_t successive_over_relaxation(Heat * const h, Phase * const p);

int continue_loop(Heat * const h, const Phase * const p);

int check_convergence(Heat * const h, const Phase * const p);

void update_beads_T(Heat * const h, const Phase * const p);

void calc_fields_T(Heat  *const h, const Phase *const p);

void calc_theta(Heat * const h, const Phase * const p);

void calc_nu(Heat * const h, const Phase * const p);

#endif//HEAT_SOR_H
