/* Copyright (C) 2017-2018 Ludwig Schneider
Copyright (C) 2017 De-Wen Sun

This file is part of SOMA.

SOMA is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SOMA is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with SOMA.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef HEAT_ANA_H
#define HEAT_ANA_H

/*! file heat_ana.h
\brief File declaring functions to analyse stationary temperature field
*/

void do_ana(Heat * const h, const Phase * const p);
void calc_T(Heat * const h, const Phase * const p);
void calc_q1_and_q2(Heat * const h, const Phase * const p);
void calc_qhot_and_qcold(Heat * const h, const Phase * const p);
void calc_j1_and_j2(Heat * const h, const Phase * const p);

#endif//HEAT_ANA_H
