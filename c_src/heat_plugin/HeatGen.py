#!/usr/bin/env python

import numpy as np
import h5py
import argparse

parser = argparse.ArgumentParser()
parser.add_argument('--n_types', required=True,
            action="store", dest="n_types",
            help="number of bead types", default="1")
parser.add_argument('-o', required=True,
            action="store", dest="heat",
            help="heat_filename.h5", default="heat")
args = parser.parse_args()

n_types = int(args.n_types)

# >>>>> INSERT PARAMETERS HERE >>>>>

normal_direction = 2

cold_x = 25
cold_y = 25
cold_z = 21
cold_radius = -1

hot_x = 25
hot_y = 25
hot_z = 42
hot_radius = 1

epsilon = 1e-8
dt_check = 0
omega = 1.9

kappa_bonded = np.array([[1, 1], [1, 1]], dtype = np.float)
kappa_non_bonded = np.array([[1, 1], [1, 1]], dtype = np.float)

# <<<<< INSERT PARAMETERS HERE <<<<<

if kappa_bonded.size != n_types**2:
    print('ERROR: incorrect size of kappa_bonded.')
    exit()

if kappa_non_bonded.size != n_types**2:
    print('ERROR: incorrect size of kappa_non_bonded.')
    exit()

if not np.allclose(kappa_bonded, kappa_bonded.T):
    print("ERROR: kappa_bonded matrix not symmetric!")
    exit()

if not np.allclose(kappa_non_bonded, kappa_non_bonded.T):
    print("ERROR: kappa_non_bonded matrix not symmetric!")
    exit()

f = h5py.File('{}_x{}_y{}_z{}.h5'.format(args.heat, tip_x, tip_y, tip_z), 'w')
f.create_dataset("kappa_bonded", dtype=np.float, data = kappa_bonded)
f.create_dataset("kappa_non_bonded", dtype=np.float, data = kappa_non_bonded)

d = f.create_dataset("parameters", dtype = np.intc)
d.attrs['epsilon'] = epsilon
d.attrs['dt_check'] = dt_check
d.attrs['time'] = 0
d.attrs['omega'] = np.float(omega)
d.attrs['normal_direction'] = normal_direction
d.attrs['hot_x'] = hot_x
d.attrs['hot_y'] = hot_y
d.attrs['hot_z'] = hot_z
d.attrs['hot_radius'] = hot_radius
d.attrs['cold_x'] = cold_x
d.attrs['cold_y'] = cold_y
d.attrs['cold_z'] = cold_z
d.attrs['cold_radius'] = cold_radius
f.close()
