/* Copyright (C) 2017-2018 Ludwig Schneider
Copyright (C) 2017 De-Wen Sun

This file is part of SOMA.

SOMA is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SOMA is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with SOMA.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "heat_io.h"
#include "heat_sor.h"
#include "heat_ana.h"

/*! file heat.c
\brief Helper program to apply heat flux to a configuration and finding stationary temperature field.

The program reads the "/beads" dataset of a hdf5 file
*/

int main(int argc, char *argv[])
{
    Phase phase;
    Phase *const p = &phase;
    #if ( ENABLE_MPI == 1 )
    if (MPI_Init(NULL, NULL) != MPI_SUCCESS)
    {
        fprintf(stderr, "MPI_ERROR (start)\n");
        return -1;
    }
    /* initialize MPI */

    const int error = MPI_Comm_dup(MPI_COMM_WORLD, &(p->info_MPI.SOMA_comm_world));
    if (error != MPI_SUCCESS)
    {
        fprintf(stderr, "MPI Error cannot duplicate MPI_COMM_WORLD %s:%d\n", __FILE__, __LINE__);
        return -1;
    }
    {
        int tmp;
        MPI_Comm_rank(p->info_MPI.SOMA_comm_world, &tmp);
        p->info_MPI.world_rank = tmp;
    }
    const int error2 = MPI_Comm_dup(p->info_MPI.SOMA_comm_world, &(p->info_MPI.SOMA_comm_sim));
    if (error2 != MPI_SUCCESS)
    {
        fprintf(stderr, "MPI Error cannot duplicate MPI_COMM_WORLD %s:%d\n", __FILE__, __LINE__);
        return -1;
    }
    #endif                          //ENABLE_MPI

    const int args_success = cmdline_parser(argc, argv, &(p->args));
    if (args_success < 0)
    {
        fprintf(stderr, "Process %d failed to read the cmdline. Exiting.\n", p->info_MPI.world_rank);
        return -2;
    }
    else if (args_success == 1) //Help and version output
    {
        return MPI_Finalize();
    }
    const int post_args = post_process_args(&(p->args), p->info_MPI.world_rank);
    if (post_args < 0)
    {
        fprintf(stderr, "Post processing the arguments on rank %d failed. Exiting.\n", p->info_MPI.world_rank);
        return -3;
    }

    const int mpi_init = init_MPI(p);
    if (mpi_init != 0)
    {
        fprintf(stderr, "ERROR: Unable to setup MPI %s:%d\n", __FILE__, __LINE__);
        exit(mpi_init);
    }

    if (p->args.move_type_arg == move_type_arg_TRIAL)
    {
        MPI_ERROR_CHECK(1, "ERROR: Trial move type is currently not working.");
    }

    if (p->args.pseudo_random_number_generator_arg == pseudo_random_number_generator_arg_TT800)
    {
        MPI_ERROR_CHECK(1, "ERROR: TT800 PRNG is currently not working.");
    }

    const int open_acc = set_openacc_devices(p);
    if (check_status_on_mpi(p, open_acc) != 0)
    {
        if (p->info_MPI.sim_rank == 0)
        fprintf(stderr, "ERROR: cannot set openacc devices.\n");
        finalize_MPI(&(p->info_MPI));
        return 1;
    }

    const unsigned int N_steps = p->args.timesteps_arg;
    /* read in configuration with routine from io */
    const int read = read_config_hdf5(p, p->args.coord_file_arg);
    MPI_ERROR_CHECK(read, "Cannot read coord file.");
    /* initialize phase */
    const int init = init_phase(p);
    MPI_ERROR_CHECK(init, "Cannot init values.");
    if (!p->bead_data_read)
    {

        if (p->info_MPI.sim_rank == 0)
        printf("INFO: Generating new bead initial data. "
        "If your configuration contains rings " "equilibration might take long.\n");
        const int new_beads = generate_new_beads(p);
        MPI_ERROR_CHECK(new_beads, "Cannot genrate new bead data.");
        //Reset the RNG to initial starting conditions.
        reseed(p, p->args.rng_seed_arg);
    }
    #if ( ENABLE_MPI == 1 )
    const int init_domain_chains_status = send_domain_chains(p, true);
    MPI_ERROR_CHECK(init_domain_chains_status, "Sending chains for domain decomposition failed.");
    #endif                          //ENABLE_MPI

    if (!p->args.skip_tests_flag)
    {
        const int test_p = test_particle_types(p);
        MPI_ERROR_CHECK(test_p, "Partile type test failed.");

        const int test51 = test_area51_violation(p);
        MPI_ERROR_CHECK(test51, "Area51 test failed.");

        test_area51_exact(p);

        const int indepent_sets = test_independet_sets(p);
        MPI_ERROR_CHECK(indepent_sets, "Indepent Set test failed.");

        const int chains_domain = test_chains_in_domain(p);
        MPI_ERROR_CHECK(chains_domain, "Chains in domain test failed");

        const int polytype_conversion = test_poly_conversion(p);
        MPI_ERROR_CHECK(polytype_conversion, "Polytype conversion test failed");
    }
    int stop_iteration = false;

    #if ( ENABLE_MPI == 1 )
    const int missed_chains = send_domain_chains(p, false);
    if (missed_chains != 0)
    exit(missed_chains);
    #endif                          //ENABLE_MPI

    if (!stop_iteration && !p->args.skip_tests_flag)
    {
        const int test51 = test_area51_violation(p);
        MPI_ERROR_CHECK(test51, "Area51 test failed.");

        test_area51_exact(p);

        const int chains_domain = test_chains_in_domain(p);
        MPI_ERROR_CHECK(chains_domain, "Chains in domain test failed");
    }

    Heat heat;
    Heat *const h = &heat;

    printf("INFO: Initialize heat data.\n");
    if(init_heat(h, p) != 0)
    {
        return -1;
    }

    uint64_t time = successive_over_relaxation(h, p);

    do_ana(h, p);

    char name[1024];
    sprintf(name, "%s_%s_end", p->args.coord_file_arg, p->args.final_file_arg);
    printf("INFO: Save temperature field in file %s\n", name);
    write_temperature_data(name, p, h);

    free_heat(h, p);

    free_phase(p);

    printf("Rank: %d \t polymers %ld\n", p->info_MPI.world_rank, p->n_polymers);

    finalize_MPI(&(p->info_MPI));
    if (p->info_MPI.world_rank == 0)
    printf("SOMA finished execution without errors.\n");

    return 0;
}
