/* Copyright (C) 2017-2018 Ludwig Schneider
Copyright (C) 2017 De-Wen Sun

This file is part of SOMA.

SOMA is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SOMA is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with SOMA.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <hdf5.h>
#include <stdlib.h>
#include "heat_io.h"
#include "heat_sor.h"
#include "heat_ana.h"

void create_checkpoint(Heat * const h, const Phase * const p)
{
    if(p->time >= h->t_check && h->dt_check != 0 && h->t_check > 0)
    {
        do_ana(h, p);

        char name[1024];
        sprintf(name, "%s_%s_t%lu", p->args.coord_file_arg, p->args.final_file_arg, h->time);

        printf("INFO: Save temperature field in file %s\n", name);
        write_temperature_data(name, p, h);

        printf("INFO: time = %u deviation = %lg (%lg)\n", h->time, h->deviation, h->epsilon);
        h->t_check += h->dt_check;
    }
}

int is_on_fixed_area(Heat * const h, const Phase * const p, const int cx, const int cy, const int cz, const Area * const a)
{
    const int x[3] = {cx, cy, cz};
    const int nx[3] = {p->nx, p->ny, p->nz};
    int dx[3];
    int dr2 = 0;

    for(int d = 0; d < 3; d++)
    {
        dx[d] = x[d] - a->x[d];

        // apply minimum image convention
        if(dx[d] > nx[d] / 2)
        {
            dx[d] -= nx[d];
        }
        if(dx[d] <= -nx[d] / 2)
        {
            dx[d] += nx[d];
        }

        if(h->normal_direction != d)
        {
            dr2 += dx[d] * dx[d];
        }
    }

    if(dx[h->normal_direction] == 0 && (dr2 <= a->radius * a->radius || a->radius == -1))
    {
        return 1;
    }
    else
    {
        return 0;
    }
}

int init_heat(Heat * const h, const Phase * const p)
{
    unsigned int max_n_beads = 0;
    for(unsigned int i = 0; i < p->n_polymers; i++)
    {
        const unsigned int N = p->poly_arch[p->poly_type_offset[p->polymers[i].type]];
        if(N > max_n_beads)
        {
            max_n_beads = N;
        }
    }
    h->max_n_beads = max_n_beads;

    h->beads_T = (soma_scalar_t *) malloc(p->n_polymers * h->max_n_beads * sizeof(soma_scalar_t));
    if (h->beads_T == NULL)
    {
        fprintf(stderr, "ERROR: Malloc %s:%d\n", __FILE__, __LINE__);
        return -1;
    }

    h->fields_T = (soma_scalar_t *) malloc(p->n_cells * p->n_types * sizeof(soma_scalar_t));
    if (h->fields_T == NULL)
    {
        fprintf(stderr, "ERROR: Malloc %s:%d\n", __FILE__, __LINE__);
        return -1;
    }

    h->theta = (soma_scalar_t *) malloc(p->n_cells * p->n_types * sizeof(soma_scalar_t));
    if (h->theta == NULL)
    {
        fprintf(stderr, "ERROR: Malloc %s:%d\n", __FILE__, __LINE__);
        return -1;
    }

    h->nu = (unsigned int *) malloc(p->n_cells * p->n_types * sizeof(unsigned int));
    if (h->nu == NULL)
    {
        fprintf(stderr, "ERROR: Malloc %s:%d\n", __FILE__, __LINE__);
        return -1;
    }

    h->kappa_bonded = (soma_scalar_t *) malloc(p->n_types * p->n_types * sizeof(soma_scalar_t));
    if (h->kappa_bonded == NULL)
    {
        fprintf(stderr, "ERROR: Malloc %s:%d\n", __FILE__, __LINE__);
        return -1;
    }

    h->kappa_non_bonded = (soma_scalar_t *) malloc(p->n_types * p->n_types * sizeof(soma_scalar_t));
    if (h->kappa_non_bonded == NULL)
    {
        fprintf(stderr, "ERROR: Malloc %s:%d\n", __FILE__, __LINE__);
        return -1;
    }

    h->T = (soma_scalar_t *) malloc(p->n_cells * sizeof(soma_scalar_t));
    if (h->T == NULL)
    {
        fprintf(stderr, "ERROR: Malloc %s:%d\n", __FILE__, __LINE__);
        return -1;
    }

    printf("INFO: Read heat parameters from file %s\n", p->args.final_file_arg);
    read_heat_parameters(p->args.final_file_arg, p, h);

    h->t_check = h->dt_check;

    update_self_phase(p, 0);

    // initialize temperature
    printf("INFO: Initialize temperature to 0 everywhere but the hot area. The hot area has temperature 1.\n");

    for (uint64_t i = 0; i < p->n_polymers; i++)
    {
        const unsigned int N = p->poly_arch[ p->poly_type_offset[p->polymers[i].type]];
        for (unsigned int j = 0; j < N; j++)
        {
            const unsigned int monotype = get_particle_type(p->poly_arch[ p->poly_type_offset[p->polymers[i].type] + 1 + j]);

            const unsigned int ij = get_2d_index(i, j, h->max_n_beads);
            int x, y, z;
            coord_to_cell_coordinate(p, p->polymers[i].beads[j].x, p->polymers[i].beads[j].y, p->polymers[i].beads[j].z, &x, &y, &z);
            const uint64_t cell = cell_coordinate_to_index(p, x, y, z);
            uint64_t index = cell_to_index_unified(p, cell, monotype);

            if(is_on_fixed_area(h, p, x, y, z, &h->hot_area))
            {
                h->beads_T[ij] = 1.;
            }
            else
            {
                h->beads_T[ij] = 0.;
            }
        }
    }

    calc_nu(h, p);
    calc_fields_T(h, p);
    calc_theta(h, p);

    h->epsilon_ana = sqrt(h->epsilon);

    return 0;
}

int free_heat(Heat *const h, const Phase *const p)
{
    free(h->beads_T);
    free(h->fields_T);
    free(h->theta);
    free(h->nu);
    free(h->kappa_bonded);
    free(h->kappa_non_bonded);
    free(h->T);

    return 0;
}

int read_heat_parameters(const char * const filename, const Phase *const p, Heat *const h)
{
    hid_t file_id = H5Fopen(filename, H5F_ACC_RDONLY, H5P_DEFAULT);

    read_attribute(file_id, &h->omega, "omega", H5T_SOMA_NATIVE_SCALAR);
    read_attribute(file_id, &h->dt_check, "dt_check", H5T_NATIVE_UINT);
    read_attribute(file_id, &h->epsilon, "epsilon", H5T_SOMA_NATIVE_SCALAR);
    read_attribute(file_id, &h->normal_direction, "normal_direction", H5T_NATIVE_INT);
    read_attribute(file_id, &h->hot_area.x[0], "hot_x", H5T_NATIVE_INT);
    read_attribute(file_id, &h->hot_area.x[1], "hot_y", H5T_NATIVE_INT);
    read_attribute(file_id, &h->hot_area.x[2], "hot_z", H5T_NATIVE_INT);
    read_attribute(file_id, &h->hot_area.radius, "hot_radius", H5T_NATIVE_INT);
    read_attribute(file_id, &h->cold_area.x[0], "cold_x", H5T_NATIVE_INT);
    read_attribute(file_id, &h->cold_area.x[1], "cold_y", H5T_NATIVE_INT);
    read_attribute(file_id, &h->cold_area.x[2], "cold_z", H5T_NATIVE_INT);
    read_attribute(file_id, &h->cold_area.radius, "cold_radius", H5T_NATIVE_INT);
    read_attribute(file_id, &h->time, "time", H5T_NATIVE_UINT);

    read_dataset(file_id, h->kappa_bonded, "kappa_bonded", H5T_SOMA_NATIVE_SCALAR);
    read_dataset(file_id, h->kappa_non_bonded, "kappa_non_bonded", H5T_SOMA_NATIVE_SCALAR);

    H5Fclose(file_id);
}

void read_dataset(hid_t file_id, void *data, const char *name, hid_t dtype_id)
{
    hid_t dataset_id = H5Dopen2(file_id, name, H5P_DEFAULT);
    H5Dread(dataset_id, dtype_id, H5S_ALL, H5S_ALL, H5P_DEFAULT, data);
    H5Dclose(dataset_id);
}

void read_attribute(hid_t file_id, void *data, const char *name, hid_t dtype_id)
{
    hid_t dataset_id = H5Dopen2(file_id, "parameters", H5P_DEFAULT);
    hid_t attribute_id = H5Aopen(dataset_id, name, H5P_DEFAULT);
    H5Aread(attribute_id, dtype_id, data);
    H5Aclose(attribute_id);
    H5Dclose(dataset_id);
}

int write_temperature_data(const char*const filename, const Phase *const p, Heat *const h)
{
    hsize_t dims[4] = {p->n_types, p->nx, p->ny, p->nz}, d1[1] = {1}, dims_types_2[2] = {p->n_types, p->n_types}, dims_types[1] = {p->n_types}, dimsp[1] = {p->n_polymers * h->max_n_beads}, dims4[4] = {p->nx, p->ny, p->nz, 3}, dims3[3] = {p->nx, p->ny, p->nz};
    int dummy = 0;

    hid_t file_id = H5Fcreate(filename, H5F_ACC_TRUNC, H5P_DEFAULT, H5P_DEFAULT);
    // write_dataset(file_id, h->beads_T, "T", 1, dimsp, H5T_SOMA_NATIVE_SCALAR);
    // write_dataset(file_id, p->fields_32, "density", 4, dims, H5T_NATIVE_UINT);
    write_dataset(file_id, h->T, "T", 3, dims3, H5T_SOMA_NATIVE_SCALAR);
    write_dataset(file_id, h->kappa_bonded, "kappa_bonded", 2, dims_types_2, H5T_SOMA_NATIVE_SCALAR);
    write_dataset(file_id, h->kappa_non_bonded, "kappa_non_bonded", 2, dims_types_2, H5T_SOMA_NATIVE_SCALAR);
    write_dataset(file_id, &dummy, "parameters", 1, d1, H5T_NATIVE_INT);
    write_attribute(file_id, "parameters", &p->nx, "nx", 1, d1, H5T_NATIVE_UINT);
    write_attribute(file_id, "parameters", &p->ny, "ny", 1, d1, H5T_NATIVE_UINT);
    write_attribute(file_id, "parameters", &p->nz, "nz", 1, d1, H5T_NATIVE_UINT);
    write_attribute(file_id, "parameters", &p->Lx, "Lx", 1, d1, H5T_SOMA_NATIVE_SCALAR);
    write_attribute(file_id, "parameters", &p->Ly, "Ly", 1, d1, H5T_SOMA_NATIVE_SCALAR);
    write_attribute(file_id, "parameters", &p->Lz, "Lz", 1, d1, H5T_SOMA_NATIVE_SCALAR);
    write_attribute(file_id, "parameters", &h->omega, "omega", 1, d1, H5T_SOMA_NATIVE_SCALAR);
    write_attribute(file_id, "parameters", &h->dt_check, "dt_check", 1, d1, H5T_NATIVE_UINT);
    write_attribute(file_id, "parameters", &h->epsilon, "epsilon", 1, d1, H5T_SOMA_NATIVE_SCALAR);
    write_attribute(file_id, "parameters", &h->deviation, "deviation", 1, d1, H5T_SOMA_NATIVE_SCALAR);
    write_attribute(file_id, "parameters", &h->time, "time", 1, d1, H5T_NATIVE_UINT);
    write_attribute(file_id, "parameters", &h->normal_direction, "normal_direction", 1, d1, H5T_NATIVE_INT);
    write_attribute(file_id, "parameters", &h->hot_area.x[0], "hot_x", 1, d1, H5T_NATIVE_INT);
    write_attribute(file_id, "parameters", &h->hot_area.x[1], "hot_y", 1, d1, H5T_NATIVE_INT);
    write_attribute(file_id, "parameters", &h->hot_area.x[2], "hot_z", 1, d1, H5T_NATIVE_INT);
    write_attribute(file_id, "parameters", &h->hot_area.radius, "hot_radius", 1, d1, H5T_NATIVE_INT);
    write_attribute(file_id, "parameters", &h->cold_area.x[0], "cold_x", 1, d1, H5T_NATIVE_INT);
    write_attribute(file_id, "parameters", &h->cold_area.x[1], "cold_y", 1, d1, H5T_NATIVE_INT);
    write_attribute(file_id, "parameters", &h->cold_area.x[2], "cold_z", 1, d1, H5T_NATIVE_INT);
    write_attribute(file_id, "parameters", &h->cold_area.radius, "cold_radius", 1, d1, H5T_NATIVE_INT);
    write_dataset(file_id, &h->qhot, "q", 1, d1, H5T_SOMA_NATIVE_SCALAR);
    write_dataset(file_id, &h->q1, "q1", 1, d1, H5T_SOMA_NATIVE_SCALAR);
    write_dataset(file_id, &h->q2, "q2", 1, d1, H5T_SOMA_NATIVE_SCALAR);
    write_dataset(file_id, &h->j1, "j1", 1, d1, H5T_SOMA_NATIVE_SCALAR);
    write_dataset(file_id, &h->j2, "j2", 1, d1, H5T_SOMA_NATIVE_SCALAR);
    H5Fclose(file_id);

    return 0;
}

void write_dataset(hid_t file_id, const void *data, const char *name, int rank, const hsize_t * current_dims, hid_t dtype_id)
{
    hid_t dataspace_id = H5Screate_simple(rank, current_dims, NULL);
    hid_t dataset_id = H5Dcreate2(file_id, name, dtype_id, dataspace_id, H5P_DEFAULT, H5P_DEFAULT, H5P_DEFAULT);
    H5Dwrite(dataset_id, dtype_id, H5S_ALL, H5S_ALL, H5P_DEFAULT, data);
    H5Dclose(dataset_id);
    H5Sclose(dataspace_id);
}

void write_attribute(hid_t file_id, const char *dset_name, const void *data, const char *name, int rank, const hsize_t * current_dims, hid_t dtype_id)
{
    hid_t dataspace_id;
    if(rank == 1 && current_dims[0] == 1)
    {
        dataspace_id = H5Screate(H5S_SCALAR);
    }
    else
    {
        dataspace_id = H5Screate_simple(rank, current_dims, NULL);
    }
    hid_t dataset_id = H5Dopen2(file_id, dset_name, H5P_DEFAULT);
    hid_t attribute_id = H5Acreate2(dataset_id, name, dtype_id, dataspace_id, H5P_DEFAULT, H5P_DEFAULT);
    H5Awrite(attribute_id, dtype_id, data);
    H5Aclose(attribute_id);
    H5Sclose(dataspace_id);
    H5Dclose(dataset_id);
}

unsigned int get_2d_index(const unsigned int poly, const unsigned int mono, const unsigned int max_n_beads)
{
    return poly * max_n_beads + mono;
}

uint64_t calc_field_index(const Phase * const p, int x, int y, int z, const int type)
{
    // cast from uint to int is necessary
    const int nx = p->nx;
    const int ny = p->ny;
    const int nz = p->nz;

    // wrap to original system box
    if(x >= nx)
    {
        x -= nx;
    }
    if(x < 0)
    {
        x += nx;
    }

    if(y >= ny)
    {
        y -= ny;
    }
    if(y < 0)
    {
        y += ny;
    }

    if(z >= nz)
    {
        z -= nz;
    }
    if(z < 0)
    {
        z += nz;
    }

    const uint64_t cell = cell_coordinate_to_index(p, x, y, z);
    return cell_to_index_unified(p, cell, type);
}
