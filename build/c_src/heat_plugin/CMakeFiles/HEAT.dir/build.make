# CMAKE generated file: DO NOT EDIT!
# Generated by "Unix Makefiles" Generator, CMake Version 3.14

# Delete rule output on recipe failure.
.DELETE_ON_ERROR:


#=============================================================================
# Special targets provided by cmake.

# Disable implicit rules so canonical targets will work.
.SUFFIXES:


# Remove some rules from gmake that .SUFFIXES does not remove.
SUFFIXES =

.SUFFIXES: .hpux_make_needs_suffix_list


# Suppress display of executed commands.
$(VERBOSE).SILENT:


# A target that is always out of date.
cmake_force:

.PHONY : cmake_force

#=============================================================================
# Set environment variables for the build.

# The shell in which to execute make rules.
SHELL = /bin/sh

# The CMake executable.
CMAKE_COMMAND = /gpfs/software/juwels/stages/2019a/software/CMake/3.14.0-GCCcore-8.3.0/bin/cmake

# The command to remove a file.
RM = /gpfs/software/juwels/stages/2019a/software/CMake/3.14.0-GCCcore-8.3.0/bin/cmake -E remove -f

# Escaping for special characters.
EQUALS = =

# The top-level source directory on which CMake was run.
CMAKE_SOURCE_DIR = /p/project/chgu14/pigard/SOMA-master

# The top-level build directory on which CMake was run.
CMAKE_BINARY_DIR = /p/project/chgu14/pigard/SOMA-master/build

# Include any dependencies generated for this target.
include c_src/heat_plugin/CMakeFiles/HEAT.dir/depend.make

# Include the progress variables for this target.
include c_src/heat_plugin/CMakeFiles/HEAT.dir/progress.make

# Include the compile flags for this target's objects.
include c_src/heat_plugin/CMakeFiles/HEAT.dir/flags.make

c_src/heat_plugin/CMakeFiles/HEAT.dir/heat.c.o: c_src/heat_plugin/CMakeFiles/HEAT.dir/flags.make
c_src/heat_plugin/CMakeFiles/HEAT.dir/heat.c.o: ../c_src/heat_plugin/heat.c
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green --progress-dir=/p/project/chgu14/pigard/SOMA-master/build/CMakeFiles --progress-num=$(CMAKE_PROGRESS_1) "Building C object c_src/heat_plugin/CMakeFiles/HEAT.dir/heat.c.o"
	cd /p/project/chgu14/pigard/SOMA-master/build/c_src/heat_plugin && /gpfs/software/juwels/stages/2019a/software/GCCcore/8.3.0/bin/gcc $(C_DEFINES) $(C_INCLUDES) $(C_FLAGS) -o CMakeFiles/HEAT.dir/heat.c.o   -c /p/project/chgu14/pigard/SOMA-master/c_src/heat_plugin/heat.c

c_src/heat_plugin/CMakeFiles/HEAT.dir/heat.c.i: cmake_force
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Preprocessing C source to CMakeFiles/HEAT.dir/heat.c.i"
	cd /p/project/chgu14/pigard/SOMA-master/build/c_src/heat_plugin && /gpfs/software/juwels/stages/2019a/software/GCCcore/8.3.0/bin/gcc $(C_DEFINES) $(C_INCLUDES) $(C_FLAGS) -E /p/project/chgu14/pigard/SOMA-master/c_src/heat_plugin/heat.c > CMakeFiles/HEAT.dir/heat.c.i

c_src/heat_plugin/CMakeFiles/HEAT.dir/heat.c.s: cmake_force
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Compiling C source to assembly CMakeFiles/HEAT.dir/heat.c.s"
	cd /p/project/chgu14/pigard/SOMA-master/build/c_src/heat_plugin && /gpfs/software/juwels/stages/2019a/software/GCCcore/8.3.0/bin/gcc $(C_DEFINES) $(C_INCLUDES) $(C_FLAGS) -S /p/project/chgu14/pigard/SOMA-master/c_src/heat_plugin/heat.c -o CMakeFiles/HEAT.dir/heat.c.s

# Object files for target HEAT
HEAT_OBJECTS = \
"CMakeFiles/HEAT.dir/heat.c.o"

# External object files for target HEAT
HEAT_EXTERNAL_OBJECTS =

c_src/heat_plugin/HEAT: c_src/heat_plugin/CMakeFiles/HEAT.dir/heat.c.o
c_src/heat_plugin/HEAT: c_src/heat_plugin/CMakeFiles/HEAT.dir/build.make
c_src/heat_plugin/HEAT: c_src/heat_plugin/libheat_plugin_lib.a
c_src/heat_plugin/HEAT: c_src/libsoma_lib.a
c_src/heat_plugin/HEAT: /gpfs/software/juwels/stages/2019a/software/OpenMPI/4.0.2-GCC-8.3.0/lib/libmpi.so
c_src/heat_plugin/HEAT: /gpfs/software/juwels/stages/2019a/software/HDF5/1.10.5-gompi-2019a/lib/libhdf5.so
c_src/heat_plugin/HEAT: /gpfs/software/juwels/stages/2019a/software/Szip/2.1.1-GCCcore-8.3.0/lib/libsz.so
c_src/heat_plugin/HEAT: /gpfs/software/juwels/stages/2019a/software/zlib/1.2.11-GCCcore-8.3.0/lib/libz.so
c_src/heat_plugin/HEAT: /usr/lib64/libdl.so
c_src/heat_plugin/HEAT: /usr/lib64/libm.so
c_src/heat_plugin/HEAT: /usr/lib64/libpthread.so
c_src/heat_plugin/HEAT: /gpfs/software/juwels/stages/2019a/software/HDF5/1.10.5-gompi-2019a/lib/libhdf5.so
c_src/heat_plugin/HEAT: /gpfs/software/juwels/stages/2019a/software/Szip/2.1.1-GCCcore-8.3.0/lib/libsz.so
c_src/heat_plugin/HEAT: /gpfs/software/juwels/stages/2019a/software/zlib/1.2.11-GCCcore-8.3.0/lib/libz.so
c_src/heat_plugin/HEAT: /usr/lib64/libdl.so
c_src/heat_plugin/HEAT: /usr/lib64/libm.so
c_src/heat_plugin/HEAT: /usr/lib64/libpthread.so
c_src/heat_plugin/HEAT: c_src/heat_plugin/CMakeFiles/HEAT.dir/link.txt
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green --bold --progress-dir=/p/project/chgu14/pigard/SOMA-master/build/CMakeFiles --progress-num=$(CMAKE_PROGRESS_2) "Linking C executable HEAT"
	cd /p/project/chgu14/pigard/SOMA-master/build/c_src/heat_plugin && $(CMAKE_COMMAND) -E cmake_link_script CMakeFiles/HEAT.dir/link.txt --verbose=$(VERBOSE)

# Rule to build all files generated by this target.
c_src/heat_plugin/CMakeFiles/HEAT.dir/build: c_src/heat_plugin/HEAT

.PHONY : c_src/heat_plugin/CMakeFiles/HEAT.dir/build

c_src/heat_plugin/CMakeFiles/HEAT.dir/clean:
	cd /p/project/chgu14/pigard/SOMA-master/build/c_src/heat_plugin && $(CMAKE_COMMAND) -P CMakeFiles/HEAT.dir/cmake_clean.cmake
.PHONY : c_src/heat_plugin/CMakeFiles/HEAT.dir/clean

c_src/heat_plugin/CMakeFiles/HEAT.dir/depend:
	cd /p/project/chgu14/pigard/SOMA-master/build && $(CMAKE_COMMAND) -E cmake_depends "Unix Makefiles" /p/project/chgu14/pigard/SOMA-master /p/project/chgu14/pigard/SOMA-master/c_src/heat_plugin /p/project/chgu14/pigard/SOMA-master/build /p/project/chgu14/pigard/SOMA-master/build/c_src/heat_plugin /p/project/chgu14/pigard/SOMA-master/build/c_src/heat_plugin/CMakeFiles/HEAT.dir/DependInfo.cmake --color=$(COLOR)
.PHONY : c_src/heat_plugin/CMakeFiles/HEAT.dir/depend

